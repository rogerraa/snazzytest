# Product basics

Here is a good place to introduce some of the basic functionality a beginner needs to know when getting started with your product.

## Links

Use it as a launch pad to the more detailed How-to's, Tutorials and Explanations within your documentation.

## At a glance

Remember to keep it simple. Stick to high level topics, written in short form.

## Customer success

Answer questions that guide your customers to your product's "Aha" moments.