---
# snazzyDocs - DO NOT REMOVE
sdChapterId: 28G-BAU-TXF-EFS
sdPageId: 9Q8-SXI-A47-EK3
sdPageName: 'Quick start'
sdIsVisible: '1'
sdChapterIndex: 0
sdItemIndex: 1
sdIsIndex: false
---
# Quick start

Before you can use \[your-product\], you'll need to get setup. Below you will find the quick start setup process.

## Download

[Download](https://your-product-domain.com/file.zip) \[your-product\] to get the compiled CSS and JavaScript.

## Snippet

Install the following snippet of Javascript right before the closing `</body>` tag.

```
<!-- [your-product] -->
<script type="text/javascript">
 (function() [
  //.. your code
 ])();
</script>
<!-- end [your-product] -->
```

## NPM

Install \[your-product\] in your Node.js powered apps with the [npm package](https://www.npmjs.com/package/[your-product]):

```
npm install [your-product]@1.0.0
```

</body>