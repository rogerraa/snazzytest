---
# snazzyDocs - DO NOT REMOVE
sdChapterId: WF9-XJ1-3K5-SWA
sdPageId: 6J6-D6K-FYQ-5L2
sdPageName: 'Guide tips'
sdIsVisible: '1'
sdChapterIndex: 2
sdItemIndex: 0
sdIsIndex: false
---
# Guide Tips

How-to guides help a user achieve a goal. They show a user how to solve specific or common problems they might face within your product. The best way to structure how-to guides are to take a user through the steps involved, with directions to solve an end goal. For example, how to import contacts.

When you are writing how-to guides, think of a user that already knows some of the basics of your app. This is a bit different than tutorials, so be careful not to confuse to purposes and intended end user of each.

## Name them well

The title of a how-to document should tell the user exactly what it does.

## Provide a series of steps

How-to guides must contain a list of steps, that need to be followed in order.

## Focus on results

How-to guides must focus on achieving a practical goal.

## Solve a problem

A how-to guide must address a specific question or problem.

## Don't explain concepts

A how-to guide should not explain things.

## Leave things out

Practical usability is more valuable than completeness.