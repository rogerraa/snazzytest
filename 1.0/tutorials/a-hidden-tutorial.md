---
# snazzyDocs - DO NOT REMOVE
sdChapterId: VFE-VID-NZU-TEL
sdPageId: TCE-SZE-YMD-OQH
sdPageName: 'A hidden tutorial'
sdIsVisible: ''
sdChapterIndex: 1
sdItemIndex: 2
sdIsIndex: false
---
# A hidden tutorial

This page is hidden from the public. Notice how this page is slightly more transparent in the left hand menu. Here is how to hide/unhide pages.

1. [Locate page](#locate-page)
2. [Reveal Options](#reveal-options)
3. [Toggle Visibility](#toggle-visibility)

## Locate page

Find the page labeled "A hidden tutorial" on the left hand side menu.

## Reveal options

Hover your mouse over it. Three options are revealed, *Copy link to page*, *Set visibility*, and *Delete*.

## Toggle visibility

Click the eye icon to toggle the visibility of a page.

&lt;call-out&gt;

Hidden pages will be shown during previews

&lt;/call-out&gt;