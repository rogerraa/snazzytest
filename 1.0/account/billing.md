---
# snazzyDocs - DO NOT REMOVE
sdChapterId: BWT-PTH-RXI-YY3
sdPageId: PIB-PAM-EOH-UV0
sdPageName: Billing
sdIsVisible: '1'
sdChapterIndex: 3
sdItemIndex: 1
sdIsIndex: false
---
# Billing

It's important to be transparent about your billing policies. Tell your customers what they can expect regarding this topic. Here we will use a basic table written in markdown that outlines some of the common questions customers have concerning billing.

## Summary

| Topic | Policy |

| ---------- |----------|

| Billing Cycle | Payments are due the first day of your billing cycle |

| Refunds | Yes/No |

| Payment Methods | Visa, MasterCard, American Express, Discover |

| Payment Currency | USD only |

| Invoice Currency | USD only |