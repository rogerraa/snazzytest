---
# snazzyDocs - DO NOT REMOVE
sdChapterId: BWT-PTH-RXI-YY3
sdPageId: 2FG-NTC-8ZU-HC9
sdPageName: Profile
sdIsVisible: '1'
sdChapterIndex: 3
sdItemIndex: 0
sdIsIndex: false
---
# Profile

## Resetting password

There are two ways to you can reset your password.

1. [Forgot password](#forgot-password)
2. [Update password](#update-password)

## Forgot password

Head to the login page and click the [Forgot password](https://change-link.url) link. A password reset link will be emailed to the email address you used to register your account.

## Update password

While logged in, open the [Settings](https://change-link.url) menu, and then [Profile](https://change-link.url). There you can enter your current password and the new password you would like to use.

## Change email address

Click on the **Settings** menu item, then **Profile**. Enter the new email you would like to use.