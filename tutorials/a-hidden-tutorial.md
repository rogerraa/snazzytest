# A hidden tutorial

This sub section is hidden from the public. Notice how this sub section is slightly more transparent in the left hand menu. Here is how to hide/unhide sub sections.

1. [Locate Sub Section](#locate-sub-section)
2. [Reveal Options](#reveal-options)
3. [Toggle Visibility](#toggle-visibility)

## Locate sub section

Find the sub section labeled "A hidden tutorial" on the left hand side menu.

## Reveal options

Hover your mouse over it. Three options are revealed, *Copy link to sub section*, *Set visibility*, and *Delete*.

## Toggle visibility

Click the eye icon to toggle the visibility of a sub section.

<call-out>

Hidden sections will be shown during previews

</call-out>