# Another tutorial

Put yourself in the shoes of an absolute beginner to your app. They are learning oriented, with the specific mind set of instructing a newcomer exactly what to do next. You are teaching them how to do a certain task through a series of steps.

1. [First Step](#first-step)
2. [Second Step](#second-step)
3. [Third Step](#third-step)

## First step

Here you explain how to do step one. The point is to get your customer started on their journey, not to get them to a final destination.

## Second step

How to do step two. If your customer has to do complicated things for two pages, that’s much too long.

## Third step

Step three. Focus on concrete steps, not abstract concepts. Only include steps a user needs to take. Include necessary code to help them see the result.

```
<pre class="ql-syntax" spellcheck="false">npm install @myorg/mypackage@latest
```